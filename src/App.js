import { HashRouter, Routes, Route } from "react-router-dom";
import MainManu from "./pages/MainManu";

export default function App() {
  return (
    <HashRouter>
      <Routes>
        <Route path="/">
          <Route index element={<MainManu/>} />
          <Route path="main" element={<MainManu/>} />
          <Route path="*" element={<MainManu/>} />
        </Route>
      </Routes>
    </HashRouter>
  );
}