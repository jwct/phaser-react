export const COLOR = {
  BACKGROUND: {
    RED: "#ea002a",
    WHITE: "#FFFFFF",
    DARK_GREEN: "#566f5a",
    BLACK: "#000000",
  },
};

export const BORDER = {
  BLACK: "1px black solid",
  YELLOW: "1px yellow solid",
  NONE: "",
};

export const SCREEN_SETTING = {
  WIDTH: 800,
  HEIGHT: 1600,
  ASPECT_RATIO: 800 / 1600, // WIDTH / HEIGHT
};
