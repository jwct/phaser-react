import React from "react";
import { Box } from "@mui/material";

import Phaser from "phaser";

import { COLOR, BORDER, SCREEN_SETTING } from "../../common/constant";

function preload() {
  // background
  this.load.image("background", "assets/images/background.jpg");
  this.load.svg("gameBanner", "assets/images/Game_banner.svg");

  // sprite 
  this.load.multiatlas('topbar', 'assets/images/TopBar_Chi_sprite.json', 'assets/images');
}

function create() {
  var currentY = 0;

  // set background
  var bg = this.add.tileSprite(
    SCREEN_SETTING.WIDTH / 2,
    SCREEN_SETTING.HEIGHT / 2,
    SCREEN_SETTING.WIDTH / 1,
    SCREEN_SETTING.HEIGHT / 1,
    "background"
  );
  //bg.setScale(2);
  var gameBanner = this.add.image(
    SCREEN_SETTING.WIDTH / 2,
    SCREEN_SETTING.HEIGHT / 2,
    "gameBanner"
  );
  gameBanner.y = gameBanner.height / 2;
  gameBanner.setScale(SCREEN_SETTING.WIDTH / gameBanner.width);
  currentY += gameBanner.height * gameBanner.scale;
  
  var topbar = this.add.sprite(
    SCREEN_SETTING.WIDTH / 2,
    SCREEN_SETTING.HEIGHT / 2, 'topbar', 'TopBar_Chi_sprite.png');
  topbar.y = currentY + topbar.height / 2;
  topbar.setScale(SCREEN_SETTING.WIDTH/topbar.width);
  currentY += topbar.height * topbar.scale;
  const frameNames = this.anims.generateFrameNames('topbar', {
    start: 1, end: 25, zeroPad: 2,
    prefix: 'frame_', suffix: '.png'
  });
  this.anims.create({ key: 'topbarAnims', frames: frameNames, frameRate: 7, repeat: -1 });
  topbar.anims.play('topbarAnims');

}

export default function MainManu() {
  const enableGame = true;

  const phaserGameRef = React.useRef(null);

  let game;
  const gameConfig = {
    type: Phaser.AUTO,
    width: SCREEN_SETTING.WIDTH,
    height: SCREEN_SETTING.HEIGHT,
    parent: "phaser-parent",
    physics: {
      default: "arcade",
      arcade: {
        gravity: { y: 200 },
      },
    },
    scene: {
      preload: preload,
      create: create,
    },
    scale: {
      // Fit to window
      mode: Phaser.Scale.FIT,
      // Center vertically and horizontally
      autoCenter: Phaser.Scale.CENTER_BOTH,
    },
  };

  React.useEffect(() => {
    // load game after react comoponent mounted
    if (enableGame) {
      if (phaserGameRef.current) {
        return;
      }
      phaserGameRef.current = new Phaser.Game(gameConfig);
      return () => {
        phaserGameRef.current.destroy(true);
        phaserGameRef.current = null;
      };
    }
  }, []);

  return (
    <>
      <Box
        sx={{
          backgroundColor: COLOR.BACKGROUND.RED,
          height: "100vh",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Box
          id="phaser-parent"
          sx={{
            aspectRatio: SCREEN_SETTING.ASPECT_RATIO,
            border: BORDER.BLACK,
          }}
        ></Box>
      </Box>
    </>
  );
}
